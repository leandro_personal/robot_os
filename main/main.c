/* Hello World Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "driver/gpio.h"

#define PH1_Dir    		26			//Dire��o Motor 1
#define EN1				23			//Habilita Motor 1

#define PH2_Dir    		27			//Dire��o Motor 2
#define EN2				19			//Habilita Motor 2

#define PH3_Dir			25			//Dire��o Motor 3
#define EN3				18			//Habilita Motor 3

#define SLEEP_MOTOR    	5			//Habilita os motores
#define PWM_LIDAR		22			//Pino que controla a rota��o do Lidar

#define LED_TEST		21

#define GPIO_OUTPUT_PIN_SEL 	((1ULL<<PH1_Dir) | (1ULL<<EN1) | \
								 (1ULL<<PH2_Dir) | (1ULL<<EN2) | \
								 (1ULL<<PH3_Dir) | (1ULL<<EN3) | \
								 (1ULL<<SLEEP_MOTOR) | (1ULL<<PWM_LIDAR) | (1ULL<<LED_TEST))


void app_main()
{
    printf("Hello world!\n");

    gpio_config_t io_conf;
    //disable interrupt
    io_conf.intr_type = GPIO_PIN_INTR_DISABLE;
    //set as output mode
    io_conf.mode = GPIO_MODE_OUTPUT;
    //bit mask of the pins that you want to set,e.g.GPIO18/19
    io_conf.pin_bit_mask = GPIO_OUTPUT_PIN_SEL;
    //disable pull-down mode
    io_conf.pull_down_en = 0;
    //disable pull-up mode
    io_conf.pull_up_en = 0;
    //configure GPIO with the given settings
    gpio_config(&io_conf);


    /* Print chip information */
    esp_chip_info_t chip_info;
    esp_chip_info(&chip_info);
    printf("This is ESP32 chip with %d CPU cores, WiFi%s%s, ",
            chip_info.cores,
            (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
            (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");

    printf("silicon revision %d, ", chip_info.revision);

    printf("%dMB %s flash\n", spi_flash_get_chip_size() / (1024 * 1024),
            (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");

    gpio_set_level(PH1_Dir, 0);
    gpio_set_level(EN1, 0);

    gpio_set_level(PH2_Dir, 0);
    gpio_set_level(EN2, 0);

    gpio_set_level(PH3_Dir, 0);
    gpio_set_level(EN3, 0);

    gpio_set_level(SLEEP_MOTOR, 1);
    gpio_set_level(PWM_LIDAR, 0);
    gpio_set_level(LED_TEST, 0);

    while(1) {
        vTaskDelay(1000 / portTICK_RATE_MS);
        gpio_set_level(EN1, 0);
        gpio_set_level(EN2, 0);
        gpio_set_level(EN3, 0);
        vTaskDelay(5000 / portTICK_RATE_MS);
        gpio_set_level(EN1, 1);
        gpio_set_level(EN2, 1);
        gpio_set_level(EN3, 1);
    }
    printf("Restarting now.\n");
    fflush(stdout);
    esp_restart();
}
